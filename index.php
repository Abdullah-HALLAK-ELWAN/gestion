<?php
  session_start();
  if (isset($_SESSION['Username'])) {
    header('Location: main.php'); // Redirect to main page
  }

  include 'connect.php';
  include "header.php";

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
 		$username = $_POST['user'];
 		$password = $_POST['pass'];
 		$hashedPass = sha1($password);

      // Check if the user exist in database
      $stmt = $con->prepare("SELECT
                    ID , Username, Password
                  FROM
                    users
                  WHERE
                    Username = ?
                  AND
                    Password = ?
                  AND
                    Admin = 1
                LIMIT 1");

      $stmt->execute(array($username, $hashedPass));
      $row = $stmt->fetch();
      $count = $stmt->rowCount();

        if ($count > 0) {
          $_SESSION['Username'] = $username; // Register session name
          $_SESSION['ID'] = $row['ID']; // Register session ID
          header('Location: main.php'); // Redirect to main page
          exit();
        }
  }

 ?>

 <form class="login" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
    <h4 class="text-center">Admin Login</h4>
    <input class="form-control" type="text" name="user" placeholder="Username" autocomplete="off" />
    <input class="form-control" type="password" name="pass" placeholder="Password" autocomplete="new-password" />
    <input class="btn btn-primary btn-block" type="submit" value="Login" />
  </form>
 <?php include "footer.php"; ?>
