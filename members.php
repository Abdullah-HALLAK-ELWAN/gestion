<?php

  session_start();
  if (isset($_SESSION['Username'])) {

    include 'connect.php';
    include "header.php";
    include "navbar.php";
    //start
    $do = isset($_GET['do']) ? $_GET['do'] : 'Manage';

    if ($do == 'Manage') { //Manage Page
    // Select all users except admin
    $stmt = $con->prepare("SELECT * FROM users WHERE admin != 1 ORDER BY prenom ");
    // Execute the statement
    $stmt->execute();
    // Assign to variable
    $rows = $stmt->fetchAll();
    ?>
    <h2 class="text-center">Manage Members</h2>
    <div class="container">
      <div class="table-responsive">
        <table class="main-table manage-members text-center table table-bordered">
          <tr>
            <td>Name</td>
            <td>Last name</td>
            <td>Mail</td>
            <td>Telephone</td>
            <td>Control</td>
          </tr>
          <?php
            foreach($rows as $row) {
              echo "<tr>";
                echo "<td>" . $row['prenom'] . "</td>";
                echo "<td>" . $row['nom'] . "</td>";
                echo "<td>" . $row['mail'] . "</td>";
                echo "<td>" . $row['telephone'] . "</td>";
                echo "<td>  <a href='members.php?do=Edit&userid=" . $row['ID']." ' class='btn btn-success'> Edit</a>
                            <a href='members.php?do=Delete&userid=" . $row['ID']." ' class='btn btn-danger confirm'> Delete</a> </td>";
              echo "</tr>";
            }
          ?>
          <a href="members.php?do=Add" class="btn btn-primary"> <i class="fa fa-plus"></i> Add New Member</a>
    <?php
    }elseif($do == 'Add'){ // Add Page
      ?>
      <h2 class="text-center">Add New Member</h2>
      <div class="container">
        <form class="form-horizontal" action="?do=Insert" method="POST" enctype="multipart/form-data">


          <!-- Start prénom field -->
          <div class="form-group form-group-lg">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10 col-md-6">
              <input type="text" name="prenom" class="form-control" required="required" />
            </div>
          </div>
          <!-- End prénom field -->

          <!-- Start nom field -->
          <div class="form-group form-group-lg">
            <label class="col-sm-2 control-label">Last name</label>
            <div class="col-sm-10 col-md-6">
              <input type="text" name="nom" class="form-control"   required="required"/>
            </div>
          </div>
          <!-- End nom field -->

          <!-- Start mail field -->
          <div class="form-group form-group-lg">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10 col-md-6">
              <input type="email" name="mail" class="form-control"  required="required" />
            </div>
          </div>
          <!-- End mail field -->

          <!-- Start téléphone field -->
          <div class="form-group form-group-lg">
            <label class="col-sm-2 control-label">Telephone</label>
            <div class="col-sm-10 col-md-6">
              <input type="text" name="tele" class="form-control"   />
            </div>
          </div>
          <!-- End téléphone field -->

          <!-- Start submit field -->
          <div class="form-group form-group-lg">
            <div class="col-sm-offset-2 col-sm-10">
              <input type="submit" value="Add Member" class="btn btn-primary btn-lg" />
            </div>
          </div>
          <!-- End submit field -->
        </form>
      </div>

      <?php
    }elseif ($do == 'Insert') { //Insert page
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
          echo "<h2 class='text-center'>Insert Member</h2>";

          // Get variables from the form
          $prenom	  = $_POST['prenom'];
          $nom  	  = $_POST['nom'];
          $mail   	= $_POST['mail'];
          $tele   	= $_POST['tele'];

          // Validate the form

          $formErrors = array();


          if (empty($prenom) ) {
            $formErrors[] =  'Prenom Cant Empty';

          if (empty($nom) ) {
            $formErrors[] =  'Name Cant Empty';
          }
          if (empty($mail) ) {
            $formErrors[] =  'Mail Cant Empty';
          }
          if (empty($tele) ) {
            $formErrors[] =  'Telephone Cant Empty';
          }

          // Loop into errors array and echo it
          foreach($formErrors as $error) {
            echo  '<div class="alert alert-danger">'. $error . '</div>';
          }
        }
          // Check if there's no error proceed the update operation
          if (empty($formErrors)) {
            // Insert userinfo in database
            $stmt = $con->prepare("INSERT INTO
                          users( prenom, nom ,  mail, telephone)
                          VALUES(:prenom, :znom, :zmail,:ztelephone) ");
            $stmt->execute(array(
              'prenom' => $prenom,
              'znom' => $nom,
              'zmail' => $mail,
              'ztelephone'=> $tele
              ));

              // Echo success message
              echo "<div class='alert alert-success'>" . $stmt->rowCount() . ' Record Inserted</div>';
            }
            } else {
              echo "<div class='alert alert-dangr'>" . ' There Is Probleme</div>' ;
            }

      }elseif ($do == 'Edit') { //Edit age

        $userid = isset($_GET['userid']) && is_numeric($_GET['userid']) ? intval($_GET['userid']) : 0 ;
        $stmt = $con->prepare("SELECT * FROM users WHERE ID = ? LIMIT 1");
        $stmt->execute(array($userid));
        $row = $stmt->fetch();
        $count = $stmt->rowCount();

        if ($count > 0) {
        ?>
          <h2 class="text-center">Edit Member</h2>
          <div class="container">
            <form class="form-horizontal" action="?do=Update" method="POST">
              <input type="hidden" name="userid" value="<?php echo $userid ?>" />
              <!-- Start prénom field -->
              <div class="form-group form-group-lg">
              <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10 col-md-6">
                  <input type="text" name="prenom" class="form-control" value="<?php echo $row['prenom']?>"/>
                </div>
              </div>
              <!-- End prenom field -->

              <!-- Start nom field -->
              <div class="form-group form-group-lg">
              <label class="col-sm-2 control-label">Last name</label>
                <div class="col-sm-10 col-md-6">
                  <input type="text" name="nom" class="form-control" value="<?php echo $row['nom']?>" />
                </div>
              </div>
              <!-- End nom field -->

              <!-- Start email field -->
              <div class="form-group form-group-lg">
              <label class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10 col-md-6">
                    <input type="text" name="email" class="form-control" value="<?php echo $row['mail']?>"/>
                  </div>
              </div>
              <!-- End email field -->

              <!-- Start téléphone Field -->
              <div class="form-group form-group-lg">
              <label class="col-sm-2 control-label">Telephone</label>
                <div class="col-sm-10 col-md-6">
                  <input type="text" name="tele" class="form-control" value="<?php echo $row['telephone']?>" />
                </div>
              </div>
              <!-- End téléphone field -->

              <!-- Start submit field -->
              <div class="form-group form-group-lg">
                <div class="col-sm-offset-2 col-sm-10">
                  <input type="submit" value="Save" class="btn btn-primary btn-lg" />
                </div>
              </div>
              <!-- End submit field -->
            </form>
          </div>

          <?php
          }else {
            echo "<div class='alert alert-dangr'>" .  ' There is no user</div>' ;
          }

        }elseif ($do == 'Update') { // Update page
          echo "<h2 class='text-center'>Update Member</h2>";
          if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            // Get variables from the form
            $id 	   = $_POST['userid'];
            $prenom  = $_POST['prenom'];
            $nom 	   = $_POST['nom'];
            $mail 	 = $_POST['email'];
            $tele 	 = $_POST['tele'];

            // Validate the form
            $formErrors = array();
            if (empty($prenom) ) {
              $formErrors[] =  'Prenom Cant Empty';
            }
            if (empty($nom) ) {
              $formErrors[] =  'Nom Cant Empty';
            }
            if (empty($mail) ) {
              $formErrors[] =  'Mail Cant Empty';
            }
            if (empty($tele) ) {
              $formErrors[] =  'Telephone Cant Empty';
            }
            foreach($formErrors as $error) {
              echo  $error . "   " . '<br/>';
            }

            // Check if there's no error proceed the update operation
            if (empty($formErrors)) {
              $stmt = $con->prepare("UPDATE users SET prenom = ?, nom = ?, mail = ?, telephone = ? WHERE ID = ?");
              $stmt->execute(array($prenom , $nom , $mail , $tele ,$id ));

                // Echo success message
              echo "<div class='alert alert-success'>" .$stmt->rowCount() . ' Record Updated';
            }
          }else {
            echo "<div class='alert alert-dangr'>" .  ' You cant access with this way </div>';
          }

        }elseif ($do == 'Delete') {
          echo "<h2 class='text-center'>Delete Member</h2>";
          echo "<div class='container'>";

          // Check if get request userid Is numeric & get the integer value of it
          $userid = isset($_GET['userid']) && is_numeric($_GET['userid']) ? intval($_GET['userid']) : 0;
          $stmt = $con->prepare("SELECT * FROM users WHERE ID = ? LIMIT 1");
          $stmt->execute(array($userid));
          $count = $stmt->rowCount();

          // If there's such ID show the form
          if ($count > 0) {
            $stmt = $con->prepare("DELETE FROM users WHERE ID = :zuser");
            $stmt->bindParam(":zuser", $userid);
            $stmt->execute();
            echo "<div class='alert alert-success'>" . $stmt->rowCount() . ' Record Deleted</div>';

          }else {
            echo "bad";
            }
        }

    include "footer.php";
}else {
  header('Location: index.php');
  exit();
}
